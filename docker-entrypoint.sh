#!/bin/sh -e
#
# Indico Docker startup script
#
# Copyright (C) 2020 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

INDICO_HOME_DIR="/opt/indico"
CONFIG_VARS="LOCAL_IDENTITIES:bool
LOCAL_GROUPS:bool
LOCAL_REGISTRATION:bool
LOCAL_MODERATION:bool
EXTERNAL_REGISTRATION_URL:str
AUTH_PROVIDERS:dict
IDENTITY_PROVIDERS:dict
PROVIDER_MAP:dict
CACHE_BACKEND:str
REDIS_CACHE_URL:str
MEMCACHED_SERVERS:list
CELERY_BROKER:str
CELERY_RESULT_BACKEND:str
CELERY_CONFIG:dict
SCHEDULED_TASK_OVERRIDE:dict
CUSTOMIZATION_DIR:str
CUSTOMIZATION_DEBUG:bool
HELP_URL:str
LOGO_URL:str
CUSTOM_COUNTRIES:dict
CUSTOM_LANGUAGES:dict
SQLALCHEMY_DATABASE_URI:str
SQLALCHEMY_POOL_SIZE:int
SQLALCHEMY_POOL_RECYCLE:int
SQLALCHEMY_POOL_TIMEOUT:int
DEBUG:bool
DB_LOG:bool
PROFILE:bool
SMTP_USE_CELERY:bool
COMMUNITY_HUB_URL:str
DISABLE_CELERY_CHECK:int
CACHE_DIR:str
LOG_DIR:str
TEMP_DIR:str
SMTP_SERVER:tuple
SMTP_LOGIN:str
SMTP_PASSWORD:str
SMTP_USE_TLS:bool
SMTP_TIMEOUT:int
NO_REPLY_EMAIL:str
PUBLIC_SUPPORT_EMAIL:str
SUPPORT_EMAIL:str
EXPERIMENTAL_EDITING_SERVICE:bool
XELATEX_PATH:str
STRICT_LATEX:bool
LOGGING_CONFIG_FILE:str
SENTRY_DSN:str
SENTRY_LOGGING_LEVEL:str
SECRET_KEY:str
SESSION_LIFETIME:int
STORAGE_BACKENDS:dict
ATTACHMENT_STORAGE:str
STATIC_SITE_STORAGE:str
BASE_URL:str
USE_PROXY:bool
ROUTE_OLD_URLS:bool
STATIC_FILE_METHOD:tuple
MAX_UPLOAD_FILE_SIZE:int
MAX_UPLOAD_FILES_TOTAL_SIZE:int
DEFAULT_LOCALE:str
DEFAULT_TIMEZONE:str
ENABLE_ROOMBOOKING:bool
PLUGINS:list
CATEGORY_CLEANUP:dict
WORKER_NAME:str
FLOWER_URL:str"

create_config() {
	for config_var in $1; do
		_var="${config_var%%:*}"
		_type="${config_var#*:}"
		_value=$(eval "echo \$$_var")
		if [ -n "$_value" ]; then
			case $_type in
				str)
					echo "${_var} = '${_value}'"
					;;
				int|bool|list|dict|tuple)
					echo "${_var} = ${_value}"
					;;
			esac
		fi
	done
}

prepare_database() {
	indico db prepare
	indico db upgrade
}

prepare_paths() {
	python_site_packages_dir="$(pip show --no-cache-dir indico | awk '/^Location: / { print $2 }')"
	install -d \
		"${INDICO_HOME_DIR}/archive" \
		"${INDICO_HOME_DIR}/cache" \
		"${INDICO_HOME_DIR}/tmp" \
		"${INDICO_HOME_DIR}/log" \
		"${INDICO_HOME_DIR}/web/static"
	cp -dr "${python_site_packages_dir}/indico/logging.yaml.sample" "${INDICO_HOME_DIR}/logging.yaml"
	cp -dr "${python_site_packages_dir}/indico/web/indico.wsgi" "${INDICO_HOME_DIR}/web/"
	cp -dr "${python_site_packages_dir}/indico/web/static" "${INDICO_HOME_DIR}/web/"
}

set_uwsgi_defaults() {
	export UWSGI_SOCKET="${UWSGI_SOCKET:-:3031}"
	export UWSGI_WSGI_FILE="${UWSGI_WSGI_FILE:-${INDICO_HOME_DIR}/web/indico.wsgi}"
	export UWSGI_UID="${UWSGI_UID:-indico}"
	export UWSGI_MASTER="${UWSGI_MASTER:-true}"
	export UWSGI_ENABLE_THREADS="${UWSGI_ENABLE_THREADS:-true}"
	export UWSGI_PROCESSES="${UWSGI_PROCESSES:-4}"
	export UWSGI_AUTO_PROCNAME="${UWSGI_AUTO_PROCNAME:-true}"
	export UWSGI_PROCNAME_PREFIX_SPACED="${UWSGI_PROCNAME_PREFIX_SPACED:-indico}"
	export UWSGI_DISABLE_LOGGING="${UWSGI_DISABLE_LOGGING:-true}"
	export UWSGI_SINGLE_INTERPRETER="${UWSGI_SINGLE_INTERPRETER:-true}"
	export UWSGI_VACUUM="${UWSGI_VACUUM:-true}"
	export UWSGI_BUFFER_SIZE="${UWSGI_BUFFER_SIZE:-20480}"
	export UWSGI_MEMORY_REPORT="${UWSGI_MEMORY_REPORT:-true}"
	export UWSGI_MAX_REQUESTS="${UWSGI_MAX_REQUESTS:-2500}"
	export UWSGI_HARAKIRI="${UWSGI_HARAKIRI:-900}"
	export UWSGI_HARAKIRI_VERBOSE="${UWSGI_HARAKIRI_VERBOSE:-true}"
	export UWSGI_POST_BUFFERING="${UWSGI_POST_BUFFERING:-1}"
	export UWSGI_RELOAD_ON_RSS="${UWSGI_RELOAD_ON_RSS:-2048}"
	export UWSGI_EVIL_RELOAD_ON_RSS="${UWSGI_EVIL_RELOAD_ON_RSS:-8192}"
}

case "$1" in
	uwsgi)
		create_config "$CONFIG_VARS" > "${INDICO_HOME_DIR}/.indico.conf"
		prepare_paths
		prepare_database
		set_uwsgi_defaults
		touch "${INDICO_HOME_DIR}/.installed-docker"
		;;
	celery)
		if [ ! -f "${INDICO_HOME_DIR}/.installed-docker" ]; then
			echo "Indico not installed yet! Exiting..." >&2
			exit 1
		fi
		shift
		set -- indico celery worker "${@:--B}"
		;;
esac

exec "$@"
