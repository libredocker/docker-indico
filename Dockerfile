# Indico Docker image
#
# Copyright (C) 2018-2020 Artur Scholz <artur.scholz@librecube.net>
# Copyright (C) 2020 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PYTHON_IMAGE_TAG=2.7.18
FROM python:${PYTHON_IMAGE_TAG}

ARG INDICO_VERSION=2.3
ARG INDICO_UID=999
ARG INDICO_HOME_DIR=/opt/indico

# Install 'uwsgi' and 'indico'
RUN pip install --no-cache-dir \
	uwsgi \
	indico==${INDICO_VERSION}

# Install 'texlive'
RUN apt-get update \
	&& apt-get install -qy --no-install-recommends \
	texlive-xetex \
	texlive-fonts-recommended \
	&& rm -r /var/lib/apt/lists/*

# Create 'indico' user and group
RUN groupadd -r -g ${INDICO_UID} indico \
	&& useradd -r -g indico -u ${INDICO_UID} -s /sbin/nologin -d ${INDICO_HOME_DIR} indico

# Prepare folders
RUN install -o indico -g indico -d ${INDICO_HOME_DIR}
VOLUME ["${INDICO_HOME_DIR}"]

# Add container entrypoint
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

# Expose uWSGI port
EXPOSE 3031

# Run application
CMD ["uwsgi"]
